import React, { Component } from 'react';
import { View, ScrollView, Alert, Text, TextInput, Image, SectionList, StyleSheet, FlatList, TouchableOpacity } from 'react-native';
import { Button, Icon, Fab, Content, SwipeRow, Toast, List, ListItem, } from 'native-base';
import IconMaterial from 'react-native-vector-icons/MaterialIcons';

import { withNavigation } from 'react-navigation';
import moment from 'moment';


import transactions from '../transactions/transactions_card.json';
console.disableYellowBox = true;


class TransactionList extends Component {
    state = {
        data: transactions.data.sort(this._sortDate),
        showToast: false
    }

    componentDidMount() {
        console.log('data', this.state.data)
        console.log('data first', this.state.data[0])

    }
    // for this.state.data sort
    _sortDate = (a, b) => {
        var dateA = new Date(a.dateTime);
        var dateB = new Date(b.dateTime);
        return dateA - dateB;
    }

    _openAlert = () => {
        alert('Create by Metryus corp.©')
    }

    // add dollar sign in front of numbers
    _isDollarTrue = (item) => {
        let itemSplit = item.toString().split('');

        if (itemSplit[0] == "-") {
            itemSplit.shift()
            itemSplit.unshift("-$")
        } else {
            itemSplit.unshift("$")
        }
        return itemSplit.join('');
    }
    _sendWhatsApp = () => {
        Toast.show({
            text: "WhatsApp",
            position: "top",
            style: { backgroundColor: "green" },
            buttonTextStyle: { color: "#fff", fontSize: 14 },
        })
    }
    _sendFacebook = () => {
        Toast.show({
            text: "Facebook",
            position: "top",
            style: { backgroundColor: "blue" },
            buttonTextStyle: { color: "#fff", fontSize: 14 },
        })
    }
    _sendMail = () => {
        Toast.show({
            text: "Gmail",
            position: "top",
            style: { backgroundColor: "red" },
            buttonTextStyle: { color: "#fff", fontSize: 14 },
        })
    }

    _deleteAlertPopup = () => {
        Alert.alert(
            'Warning!',
            'Do you want to delete this transaction?',
            [
                {
                    text: 'OK',
                    onPress: () => {
                        console.log('OK Pressed');
                        Toast.show({
                            text: "Delete only in real project. :)",
                            position: "top",
                            style: { backgroundColor: "#376fb6" },
                            buttonTextStyle: { color: "#fff", fontSize: 14 },
                        })
                    }
                },
                {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
            ],
            { cancelable: false },
        );
    }

    _keyExtractor = (item, index) => item.id;

    // render item for FlatList
    _renderItem = ({ item }) => (
        <View>
            <View style={styles.headerTwo}>
                {/* There should be a check here if new date () matches today then "today" and etc. (Today/Yesterday it's for example) */}
                {<Text>{item == this.state.data[0] ? <Text>Today</Text> : item == this.state.data[1] ? <Text>Yesterday</Text> : moment(item.dateTime).add(0, 'days').format('DD MMM')}</Text>}
                {item == this.state.data[0] ? <TouchableOpacity onPress={() => this.props.navigation.navigate('All')}>
                    <Text style={styles.headerTextTwo}>View all</Text>
                </TouchableOpacity> : null}
            </View>

            <TouchableOpacity style={styles.flatListView}>
                <Content scrollEnabled={false}>
                    <SwipeRow
                        leftOpenValue={75}
                        rightOpenValue={-75}
                        left={
                            <Button full onPress={() => alert('Transaction name: \n' + item.description + "\n" + "\n" + 'You made a transaction: \n' + moment(item.dateTime).format('LLL'))}>
                                <Icon active name="information-circle" />
                            </Button>
                        }
                        body={
                            <View style={{ display: 'flex', flexDirection: 'row' }}>
                                <View style={{ width: '20%', flexDirection: 'column', alignItems: 'center' }}>
                                    <View style={{ alignItems: 'center' }}>
                                        {Math.sign(item.amount) == -1 ? <Image
                                            source={require('../assets/arrow-top.png')}
                                        /> : <Image
                                                source={require('../assets/arrow-bot.png')}
                                            />
                                        }
                                    </View>
                                    <Text style={styles.flatListSubText}>{moment(item.dateTime).format('h:mm a')}</Text>
                                </View>
                                <View style={{ width: '55%' }}>
                                    <Text style={styles.flatListText}>{item.description}</Text>
                                    <Text style={styles.flatListSubText}>#907565648567</Text>
                                </View>
                                <View style={{ width: '25%', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={styles.flatListAmount}>{this._isDollarTrue(item.amount)}</Text>
                                </View>
                            </View>
                        }
                        right={
                            <Button danger onPress={() => this._deleteAlertPopup()}>
                                <Icon active name="trash" />
                            </Button>
                        }
                    />
                </Content>

            </TouchableOpacity>

        </View>
    )



    render() {
        const { data } = this.state;
        return (
            <View style={{ flex: 1 }} >
                <View>
                    <View style={styles.header}>
                        <Text style={styles.headerText}>Last Transaction</Text>
                        <Text style={styles.headerTextThin}>Details</Text>
                        <Text style={styles.headerTextThin}>Transfer</Text>
                        <Text style={styles.headerTextThin}></Text>
                    </View>
                </View>
                <ScrollView>
                    <View>
                        <FlatList
                            extraData={this.extraData}
                            data={data}
                            keyExtractor={this._keyExtractor}
                            renderItem={this._renderItem}
                        />
                    </View>
                </ScrollView>
                <View>
                    {/* <IconMaterial
                        name="dehaze"
                        color="#fff"
                        size={34}
                    /> */}
                    <Fab
                        active={this.state.active}
                        direction="up"
                        containerStyle={{}}
                        style={styles.btnView}
                        position="bottomRight"
                        onPress={() => this.setState({ active: !this.state.active })}>
                        <IconMaterial name="dehaze" />
                            <Button style={{ backgroundColor: '#34A34F' }} onPress={()=> this._sendWhatsApp()}>
                                <Icon name="logo-whatsapp" />
                            </Button>
                            <Button style={{ backgroundColor: '#3B5998' }} onPress={()=> this._sendFacebook()}>
                                <Icon name="logo-facebook" />
                            </Button>
                            <Button style={{ backgroundColor: '#DD5144' }} onPress={()=> this._sendMail()}>
                                <Icon name="mail" />
                            </Button>
                    </Fab>
                </View>
            </View >
        );
    }
}


// export default TransactionList;
export default withNavigation(TransactionList);


const styles = StyleSheet.create({
    header: {
        marginBottom: 10,
        paddingHorizontal: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    sectionHeader: {
        paddingTop: 2,
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 2,
        fontSize: 14,
        fontWeight: 'bold',
        backgroundColor: 'rgba(247,247,247,1.0)',
    },
    headerTwo: {
        backgroundColor: '#fff',
        paddingHorizontal: 10,
        marginVertical: 5,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    headerText: {
        fontSize: 18,
        fontFamily: 'System',
        fontWeight: '400',
        color: '#263550'
    },
    headerTextThin: {
        fontSize: 18,
        fontFamily: 'System',
        fontWeight: '100',
        color: '#263550'
    },
    headerTextTwo: {
        fontSize: 16,
        fontWeight: '400',
        color: '#263550'
    },
    flatListView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomWidth: 0,
    },
    flatListText: {
        paddingLeft: 10,
        paddingVertical: 5,
        fontSize: 16,
        backgroundColor: '#fff',
        textAlign: 'left',
        color: '#263550',
        fontFamily: 'System',
        fontWeight: '400'
    },
    flatListAmount: {
        paddingLeft: 10,
        paddingVertical: 5,
        fontSize: 18,
        backgroundColor: '#fff',
        textAlign: 'left',
        color: '#263550',
        fontFamily: 'System',
        fontWeight: '400'
    },
    flatListSubText: {
        paddingHorizontal: 10,
        paddingTop: 3,
        paddingBottom: 5,
        fontSize: 11,
        backgroundColor: '#fff',
        textAlign: 'left',
        color: '#263550',
        fontFamily: 'System',
        fontWeight: '400'
    },
    font: {
        fontSize: 26
    },
    btnView: {
        position: "absolute",
        bottom: 15,
        right: 5,
        width: 50,
        height: 50,
        paddingTop: 5,
        backgroundColor: '#2fb127',
        borderRadius: 50,
        opacity: 0.9,
        justifyContent: 'center',
        alignItems: 'center',
        shadowOffset: { width: 1, height: 1, },
        shadowColor: 'black',
        shadowOpacity: 0.5,
    },
    btnTextInput: {

    }
})