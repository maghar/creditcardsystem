import React, { Component } from 'react';
import { View, StyleSheet, FlatList } from 'react-native';
import moment from 'moment';
import { ListItem, Left, Body, Right, Thumbnail, Text } from 'native-base';

import transactions from '../transactions/transactions_card.json';


class ViewAll extends Component {
    state = {
        data: transactions.data.sort(this._sortDate),
    }

    _sortDate = (a, b) => {
        var dateA = new Date(a.dateTime);
        var dateB = new Date(b.dateTime);
        return dateA - dateB;
    }

    _keyExtractor = (item, index) => item.id;

    _renderItem = ({ item }) => (
        <View>
            <ListItem>
                <Body>
                    <Text>{item.description}</Text>
                    <Text note>{moment(item.dateTime).format('LLL')}</Text>
                </Body>
            </ListItem>
        </View>

    );

    render() {
        // const { data } = this.state
        return (
            <View>
                <View style={styles.header}>
                <Text style={{fontSize: 20}}>Transaction list</Text>
                </View>
                <FlatList
                    data={this.state.data}
                    extraData={this.state}
                    keyExtractor={this._keyExtractor}
                    renderItem={this._renderItem}
                />
            </View>
        );
    }
}

export default ViewAll;



const styles = StyleSheet.create({
    header: {display: 'flex', flexDirection: 'row', justifyContent: 'center', paddingVertical: 10}
})