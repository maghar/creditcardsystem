import React, { Component } from 'react';
import { View, Text } from 'react-native';
import CreditCard from './CreditCard';
import Transaction from './TransactionList';

class Main extends Component {
  static navigationOptions = {
    title: 'My Card',
  }

  render() {
    return (
      <View style={{
        flex: 1,
        backgroundColor: "#fff",
      }}>
        <CreditCard />
        <Transaction />
      </View>
    );
  }
}

export default Main;
