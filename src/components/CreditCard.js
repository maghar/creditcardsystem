import React, { Component } from 'react';
import { Image, Dimensions, StyleSheet } from 'react-native';
import { View, DeckSwiper, Card, CardItem, Thumbnail, Left } from 'native-base';

const cards = [
    {
        name: 'Front',
        image: require('../assets/visa.png'),
    },
    {
        name: 'Back',
        image: require('../assets/back.png'),
    },

]
class CreditCard extends Component {
    render() {
        return (
            // <Image style={styles.card}
            //     source={require('../assets/visa.png')}
            // />
            <View style={{height: '45%'}}>
                <DeckSwiper
                style={{flex: 1}}
                    dataSource={cards}
                    renderItem={item =>
                        <View style={{ elevation: 3 }}>
                                <Image style={styles.card} source={item.image} />
                        </View>
                    }
                />
            </View>
        );
    }
}

export default CreditCard;


const dimensions = Dimensions.get('window');
const imageHeight = Math.round(dimensions.width * 9 / 14);
const imageWidth = dimensions.width;

const styles = StyleSheet.create({
    card: {
        height: imageHeight,
        width: imageWidth,
        marginVertical: 10,
        alignItems: 'stretch',

    }
})