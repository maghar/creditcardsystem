import { StackNavigator } from 'react-navigation';

import Main from './components/Main';
import CreditCard from './components/CreditCard';
import TransactionList from './components/TransactionList';
import ViewAll from './components/ViewAll';

const BaseNavigation = StackNavigator({
  Main: { screen: Main },
  Card: { screen: CreditCard },
  TransactionList: { screen: TransactionList },
  All: { screen: ViewAll }
});

export default BaseNavigation;
