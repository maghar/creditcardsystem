import React from 'react';
import { Provider } from 'react-redux';
import { Root } from "native-base";


import Store from './Store';
import BaseNavigation from './Router';

const App = () => (
  <Root>
    <Provider store={Store}>
      <BaseNavigation />
    </Provider>
  </Root>

);

export default App;
