# React Native CreditCardSystem
Simple react-native boilerplate for mobile development.

## Getting Started

1. Clone this project `git clone https://maghar@bitbucket.org/maghar/creditcardsystem.git`
2. Run `npm install` from root directory
3. Start the app in [an emulator](/docs/quick-tips.md#running-in-an-emulator)
4. maybe need   npm install native-base --save
